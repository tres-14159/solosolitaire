/**
 * Program: Solo, a computer solitarie card game.
 * By: MD, md@tomatesasesinos.com
 * Copyright 2007
 *
 * License: GPL http://www.gnu.org/licences/gpl.html
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 *
 * File: globals.h
 * Contents: Constants and other things.
 */

#ifndef _GLOBALS_H_
#define _GLOBALS_H_

#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

/*Card with number and state*/
typedef struct _card card;

struct _card {
	int numCard;
	int faceCard;
};

//Const for state of card.
#define FACE_DOWN	0
#define FACE_UP		1

//Places in the board
/*
Zones as you can see in the screen
--------------
|A B   C D E F|
|             |
|G H I J K L M| N
---------------
*/

#define PILE_DRAW_OUT		0	//Letter A in upper diagram
#define PILE_CARDS_DRAW		1	//Letter B in upper diagram
#define PILE_SUIT1			2	//Letter C in upper diagram
#define PILE_SUIT2			3	//Letter D in upper diagram
#define PILE_SUIT3			4	//Letter E in upper diagram
#define PILE_SUIT4			5	//Letter F in upper diagram
#define END_PILES			6

#define COLUMN1				6	//Letter G in upper diagram
#define COLUMN2				7	//Letter H in upper diagram
#define COLUMN3				8	//Letter I in upper diagram
#define COLUMN4				9	//Letter J in upper diagram
#define COLUMN5				10	//Letter K in upper diagram
#define COLUMN6				11	//Letter L in upper diagram
#define COLUMN7				12	//Letter M in upper diagram
#define END_COLUMNS			13

#define HAND				13	//Letter N in upper diagram. But at the moment the program don't use this constant.

#define NUM_BOARD_PLACES	14

//Other constants
#define NO_CARD				-1
#define KING_CARD			12
#define NUM_CARDS_IN_DECK	52
#define NUM_SUIT_IN_DECK	4
#define NUM_CARDS_IN_SUIT	13

//Boleans consts.
#define TRUE	1
#define FALSE	0

//Global vars.
	/*In each position have card or NO_CARD.
	52 cards in each position, it is max but it's impossible to fill or overflow.*/
card board[NUM_BOARD_PLACES][NUM_CARDS_IN_DECK];
int numCardsBoardZones[NUM_BOARD_PLACES];
int numCardsFaceUpInZone[END_COLUMNS - COLUMN1]; //Number of cards in each column, cards make stair.

int deck[NUM_CARDS_IN_DECK]; //Deck, in the star it have all number cards, and in the final all NO_CARD

/*FUNCTIONS AND PROCEDURES*/
void startGame(void);
void dealCards(void);
int extractCardToDeck(int pos); //Extract a card in the position.
void putCardInZone(int zoneBoard, int card, int faceCard);
void makeSpaceInFirstPositionZone(int zoneBoard);
void extractCardPileDrawOut(void);
void deleteFirstPositionZone(int posInBoard);
void refillPileDrawOut(void);
void moveCard(int origin,int destiny);
void legalMoveCard(int origin,int destiny);
int moveCardToPileSuit(int zoneBoard);
void legalMoveGroupCards(int origin, int positionInStairFaceUp, int destiny); //move group of cards in stair
int win(void);

//Local includes
#include "graphics.h"

#endif

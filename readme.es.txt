1.Requisitos del sistema
2.Como se juega
2.1.Teclado
3.Contactar

1.Requisitos del sistema
Los requisitos actuales son:
	-GNU/Linux
	-SDL
	-SDLimage

2.Como se juega
Sigue las reglas tradicionales del solitario pero sin puntuacion.

Vas a poder mover un cursor azul para seleccionar cartas o para sacar nueva carta de la pila. Ademas de ello si haces "doble click" dando dos veces con el espacio sobre una carta, si esta puede ir a la pila de su palo se ira.

2.1.Teclado
	Cursores - Mueves el marcador azul o el rojo si tienes seleccionada una o varias cartas
	Espacio - Marcador azul, seleccionas una carta o grupo de cartas para mover, o sacas de la pila si estas sobre ella y si haces doble click sobre una carta boca arriba y puede entrar en una pila de su mazo ira a ella. Con el marcador rojo seleccionas el destino de una o un grupo de cartas y si cumple las reglas se moveran.
	Escape - Sales del programa

3.Contactar
	- tres.14159@gmail.com

4.Creditos
Desarrollador
	- MD (http://tomatesAsesinos.com)
Imagenes
	- Nicubunu (http://nicubunu.ro)
	- MD (http://tomatesAsesinos.com)

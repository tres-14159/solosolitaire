#simple makefile for the Solo Solitaires proyect.

CC = gcc
CFLAGS = $(shell pkg-config --cflags sdl2 SDL2_image) -g
LDFLAGS = $(shell pkg-config --libs sdl2 SDL2_image) -g


all: solo.o graphics.o
	$(CC) solo.o graphics.o $(LDFLAGS) -o solo
solo.o: globals.h
	$(CC) $(CFLAGS) -c solo.c
graphics.o: globals.h graphics.h
	$(CC) $(CFLAGS) -c graphics.c
clean:
	rm *.o solo

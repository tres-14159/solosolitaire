Solo - Solitarie
================

It's a simple game about the Solitarie board game. And it is make for
simple gui in libSDL and to use few keys. It is more or less a example
of programming boardgame.

1. System requirements
2. How to play?
	1. Keyboard
3. Screenshots
4. Contant

And the points are:

1. System requirements
----------------------
The actual requirements are:

- [GNU/Linux](http://en.wikipedia.org/wiki/Linux)
- [SDL](http://www.libsdl.org/)
- [SDLimage](http://www.libsdl.org/projects/SDL_image/)

Maybe the project run in other systems.

2. How to play?
---------------
This game is same to traditional solitarie althought it haven't count
points.

3. Screenshots
--------------

![](https://gitlab.com/tres-14159/solosolitaire/raw/master/screenshots/screenshot_solo_in_game.png)

![](https://gitlab.com/tres-14159/solosolitaire/raw/master/screenshots/screenshot_solo_start_game.png)

[[IN TRANSLATION]]

[[BELLOW IN SPANISH]]

1. Requisitos del sistema
2. Como se juega
	1. Teclado
3. Contactar
4. Donaciones
5. Creditos

1.Requisitos del sistema
------------------------
Los requisitos actuales son:
- GNU/Linux
- SDL
- SDLimage

2.Como se juega
---------------
Sigue las reglas tradicionales del solitario pero sin puntuacion.

Vas a poder mover un cursor azul para seleccionar cartas o para sacar
nueva carta de la pila. Ademas de ello si haces "doble click" dando dos
veces con el espacio sobre una carta, si esta puede ir a la pila de su
palo se ira.

2.1.Teclado
-----------
- Cursores - Mueves el marcador azul o el rojo si tienes seleccionada
  una o varias cartas
- Espacio - Marcador azul, seleccionas una carta o grupo de cartas para
  mover, o sacas de la pila si estas sobre ella y si haces doble click
  sobre una carta boca arriba y puede entrar en una pila de su mazo ira
  a ella. Con el marcador rojo seleccionas el destino de una o un grupo
  de cartas y si cumple las reglas se moveran.
- Escape - Sales del programa

3.Contactar
-----------
- <tres.14159@gmail.com>

4.Donativos
-----------
Este juego es software libre pero siempre puedes colaborar el
desarrollador, en el [Patreon de Miguel de Dios](https://www.patreon.com/migueldedios)

![](http://www.tomatesasesinos.com/wp-content/uploads/2020/01/become_a_patron_button.png)

5.Creditos
----------
- Desarrollador
	- [MD](http://tomatesAsesinos.com)
- Imagenes
	- [Nicubunu](http://nicubunu.ro)
	- [MD](http://tomatesAsesinos.com)

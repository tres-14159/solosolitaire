/**
 * Program: Solo, a computer solitarie card game.
 * By: MD, md@tomatesasesinos.com
 * Copyright 2007
 *
 * License: GPL http://www.gnu.org/licences/gpl.html
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 *
 * File: graphics.h
 * Contents: Constants, function heads and other things
 */

#ifndef _GRAPHICS_H_
#define _GRAPHICS_H_

#include "globals.h"

//Constants for graphics and paint (all measure in pixels).
#define SCREEN_BORDER 10 // Not less than WIDTH_BORDER_PILES_SUIT + MARGIN_BETWEEN_CARDS_AND_BORDER_PILES_SUIT

#define CARD_WIDTH 50
#define CARD_HEIGHT 68

#define MARGIN_BETWEEN_CARDS 20 // Not less than WIDTH_BORDER_PILES_SUIT + MARGIN_BETWEEN_CARDS_AND_BORDER_PILES_SUIT
#define VERTICAL_MARGIN_BETWEEN_CARDS 23
#define MARGIN_BETWEEN_CARD_FACE_DOWN_CARD 5

#define WIDTH_BORDER_PILES_SUIT 1
#define MARGIN_BETWEEN_CARDS_AND_BORDER_PILES_SUIT 2

#define MARGIN_BETWEEN_CARDS_IN_PILE 2

//Constants for images files.
#define FILE_IMG_CARDS "data/cardsOrnamental.png"
#define FILE_IMG_BACKFACE_CARD "data/backCard00.png"
#define FILE_IMG_WIN_TEXT "data/winText.png"
#define FILE_IMG_EMPTY_TEXT "data/emptyText.png"

#define FILE_IMG_SEL_RED "data/selectionRed.png"
#define FILE_IMG_SEL_BOTTOMCUP_RED "data/seleccionRed_bottom_cup.png"
#define FILE_IMG_SEL_TOPCUP_RED "data/seleccionRed_top_cup.png"
#define FILE_IMG_SEL_PIECE_RED "data/seleccionRed_piece.png"

#define FILE_IMG_SEL_BLUE "data/seleccionBlue.png"
#define FILE_IMG_SEL_BOTTOMCUP_BLUE "data/seleccionBlue_bottom_cup.png"
#define FILE_IMG_SEL_TOPCUP_BLUE "data/seleccionBlue_top_cup.png"
#define FILE_IMG_SEL_PIECE_BLUE "data/seleccionBlue_piece.png"

//Screen resolution constans
#define SCREEN_W 640
#define SCREEN_H 480

//Color's constants (in RGBA)
#define COLOR_BACKGROUND_BOARD 0,255,0,255
#define COLOR_BORDER_RECTANGLE_PILES_SUIT 255,255,255, 255

//Constants for move cursor
#define UP 0
#define DOWN 1
#define RIGHT 2
#define LEFT 3

//macros for packet posCursor and depacket
#define POSITION_CURSOR(POS_IN_STAIR_FACE_UP,POS_IN_ZONE_BOARD) ((100 * POS_IN_STAIR_FACE_UP) + POS_IN_ZONE_BOARD)
#define POSITION_CURSOR_TO_POS_IN_ZONE_BOARD(POS_CURSOR) (POS_CURSOR % 100)
#define POSITION_CURSOR_TO_POS_IN_STAIR_FACE_UP(POS_CURSOR) (POS_CURSOR / 100)

//Constants for paint cursors
#define CURSOR_MOVE 0
#define CURSOR_ACTION 1 

//global vars
SDL_Texture* defaultCursorSurface;
SDL_Texture* defaultCursorSurface_bottomcup;
SDL_Texture* defaultCursorSurface_topcup;
SDL_Texture* defaultCursorSurface_piece;
	//Surface and cords for rectangula cursor origin of action.
SDL_Texture* originActionCursorSurface;
SDL_Texture* originActionCursorSurface_bottomcup;
SDL_Texture* originActionCursorSurface_piece;
SDL_Texture* originActionCursorSurface_topcup;

/*
	possibleZonesCursorInBoard: Positions avaliable for cursor, that It change in any moment. The values
	in cell are
		0) for not put cursor in this zone
		1) for yes put cursor in this zone, and it represent the one card in this stair o card in face down
		other numbers) the cursor move across stair because there is a stair in this zone

	BUT the program don't use the variable....at the moment (TODO)*/
int possibleZonesCursorInBoard[NUM_BOARD_PLACES];
int posCursor; // Packet two values in one var, as 100 * POS_IN_STAIR_FACE_UP + POS_IN_ZONE_BOARD
int posInitActionCursor;

int actionInPogress;

SDL_Window *window;
SDL_Renderer *renderer;


SDL_Surface* temp_image;

SDL_Texture *cardsSurface, *backFaceCardSurface,*emptyTextSurface, *winTextSurface;

void startCursors(void);
void loadImages(void);
void startSDL(void);
void startWindow(void);
SDL_Rect *numCartToSprite(int numCard);
void blitFirstCard(int zoneBoard, int x,int y); //For blit know the margin of screen.
void showBoard(void);
void paintBorderRectangleOfPilesSuit(void);
void paintPiles(void);
void paintStairPileCardUp(void);
void blitCard(int zoneBoard,int posInStairPile,int x,int y);
void solitaireSDL(void);
void paintCursors(void);
void paintCursor(int cursor);
void moveBoxPointerKeyboard(int direction);
void action(void);
void actionExtract(void);

SDL_Texture* loadImageSDL2(char* file);

#endif
